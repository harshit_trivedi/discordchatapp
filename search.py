from abc import ABC, abstractmethod
import constants
from data import ExtractLink, ExtractMethod
from search_backend import redis_conn
from googlesearch import search


class SearchQuery:

    @staticmethod
    def save(search_keyword):
        search_keyword = "_".join([key for key in search_keyword.split(' ') if key != ''])
        redis_conn.set(search_keyword, 1)


class SearchHistory:

    @staticmethod
    def get(keyword):
        keywords = [key for key in keyword.split(' ') if key != '']
        history = []
        for word in keywords:
            word = '*%s*' % (word)
            keys = redis_conn.keys(word)
            history += keys
        return SearchHistory.create_readble_respone(history)

    @staticmethod
    def create_readble_respone(history):
        if not history:
            return None
        response = "Results: "
        for result in history:
            result = " ".join(str(result.decode("utf-8")).split('_'))
            response += "%s \n" % (str(result).strip())
        return response


class AbstractSearch(ABC):
    endpoint = ""

    @abstractmethod
    def do_search(self, keyword, extract_method):
        raise NotImplementedError(
            "Please implement get method inside your search subclass"
        )


class GoogleSearch(AbstractSearch):

    def do_search(self, query, extract_method):
        if extract_method == ExtractMethod.LINK:
            return ExtractLink().get(search(query, stop=constants.NUMBER_OF_LINK_PER_PAGE))
        return ""


g_search = GoogleSearch()
