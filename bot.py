import discord
from dotenv import load_dotenv
import constants

from data import ExtractMethod
from search import g_search, SearchQuery, SearchHistory


class DiscordClient:

    def __init__(self):
        load_dotenv()

    def start(self):
        client = discord.Client()
        return client


client = DiscordClient().start()


@client.event
async def on_ready():
    print(f'{client.user} has connected to Discord!')


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.split(' ')[0] == '!google':
        content = " ".join(message.content.split(' ')[1:])
        SearchQuery.save(content)
        response = g_search.do_search(content, extract_method=ExtractMethod.LINK)
        if response:
            await message.channel.send(response)

    if message.content.split(' ')[0] == '!recent':
        content = " ".join(message.content.split(' ')[1:])
        response = SearchHistory.get(content)
        if response:
            await message.channel.send(response)

    if message.content.lower() == 'hi':
        response = "hey"
        await message.channel.send(response)


client.run(constants.DISCORD_BOT_TOKEN)
