from abc import ABC, abstractmethod
from urllib.parse import unquote 

class ExtractMethod:
    LINK = "LINK"


class ExtractData(ABC):

    @abstractmethod
    def get(self, data):
        raise NotImplementedError(
            "Please implement get method inside your Extract Data Subclass"
        )


class ExtractLink(ExtractData):
    method = 'LINK'

    def get(self, data):
        results = []
        for result in data:
            results.append(result)
        return self.create_readble_respone(results)

    def create_readble_respone(self, data):
        response = ""
        for link in data:
            response += "%s: %s\n" % (self.method, unquote(link))
        return response
