import redis
from constants import REDIS_PORT, REDIS_HOST, REDIS_DB


# create connection to redis

class RedisWrapper(object):
    shared_state = {}

    def __init__(self):
        self.__dict__ = self.shared_state

    def get_connection(self):
        return self.get

    def get(self, CACHES_TYPE="default"):
        rconn = self.shared_state.get(CACHES_TYPE, "FALSE")
        if rconn == "FALSE":
            rconn = self.connect()
            self.shared_state[CACHES_TYPE] = rconn
            return rconn
        return rconn

    def connect(self):
        connection_pool = redis.ConnectionPool(host=REDIS_HOST, port=REDIS_PORT,
                                               db=REDIS_DB)
        rconn = redis.StrictRedis(connection_pool=connection_pool)
        print("Redis connection created")
        return rconn


redis_conn = RedisWrapper().get_connection()()
